var db = require('mongojs')("localhost/niot", ['light_alarm']);
const { Board, Thermometer } = require("johnny-five");
const board = new Board();
const express = require('express')
const app = express()
var lastTrigger = null;

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function process(volt) {
    console.log('event!');
    db.light_alarm.insert({
        event: 'alarm',
        volt: volt,
        eventDate: now
    });
}
app.listen(3000, function () {
    console.log('Le serveur est dispo sur le port 3000')
})


var temperature = 0
board.on("ready", () => {
    const thermometer = new Thermometer({
      controller: "LM335",
      pin: "A0"
    });
  
    //Quand la température change : Affiche sur la vue la température.
    thermometer.on("change", () => {
      const {celsius, fahrenheit, kelvin} = thermometer;
      temperature = celsius;
    });
});
  
app.get('/', function (req, res) {
    temperature = getRandomInt(28);
    res.send('<h1>temperature : ' + temperature + ' °C </h1>');
})